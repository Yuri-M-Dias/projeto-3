 /* Projeto 3 de Sistemas Operacionais
 * Alunos: Vinicius Carvalho Machado e Yuri Matheus Dias
 */
 #include <stdio.h>
 #include <stdlib.h>
 /* Aqui sim e a importacao pro sqlite, lembrar de linkar na compilacao tambem. */
 #include "sqlite3.h"
 #include <pthread.h>
 #include <time.h>
 #include <unistd.h>
 #include <semaphore.h>
 #include <string.h>
 #include <signal.h>
 #include <sys/types.h>
 
 typedef struct{
 	char* querry;
 	int tabelatolock;
 	int colunatolock[2];
 }Querry_struct;
 
 char* querryini = "create table NERV (EVAno INTEGER PRIMARY KEY,pilot TEXT);create table SEELE (EVAno INTEGER PRIMARY KEY,pilot TEXT);create table WILLE (EVAno INTEGER PRIMARY KEY,pilot TEXT);";
 char* querryfim = "select * from NERV;select * from SEELE;select * from WILLE;";
 char* tables[] = {"NERV", "SEELE", "WILLE"};
 char* colunas[] = {"EVAno", "pilot" , "*"};
 char* pilots[] = {"Shinji", "Asuka", "Rei", "Mari", "Kaworu"};
 char* EVAs[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8"};
 char* writerquerries[] = {"INSERT INTO ","CREATE TABLE ","UPDATE ", "DROP TABLE "};
 char* writerquerrieshelper[] = {" VALUES ", " ", " SET ", " "};
 char* create_table_atributes = " (EVAno INTEGER PRIMARY KEY,pilot TEXT)";
 char* wherehelper[] = {" >= ", " > ", " < ", " <= "};

pthread_mutex_t esperaEscritores = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t contadorLeitores = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t contadorEscritores = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t esperaLeitores = PTHREAD_MUTEX_INITIALIZER;
int numeroDeLeitores=0,numeroDeEscritores=0;

 pthread_mutex_t mutex_noReaders = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t mutex_noWriters = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t noReaders = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t noWriters = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t mutex_time_data_reader = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t mutex_time_data_writer = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t mutex_exec = PTHREAD_MUTEX_INITIALIZER;
 pthread_mutex_t mutex_general[3][2];
 sqlite3 *db;
 sem_t espera_inicio_thread, espera_fim_thread;
 int contadorVezesWriter = 0;
 int contadorVezesReader = 0;
 int tantosReader, tantosWriter;
 int numReader = 0, numWriter = 0;
 int varCond = 0;
 int num_colunas = 3;
 int opcao_ts;
 double mediaWriter = 0, mediaReader = 0, maxReader = 0, maxWriter = 0, minReader = 0, minWriter = 0;
 
 /* Essa e a funcao e o handler do que o sqlite faz, o que ele executa.
  * Ele pega os argumentos de retorno do sqlite e imprime os resultado de SHOW/SELECT e tal
  */
 int callback(void *NotUsed, int argc, char **argv, char **azColName){
 	int i;
 	for(i=0; i<argc; i++){
 		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
 	}
 	printf("\n");
 	return 0;
 }
 
 void espera_tempo() {
 	useconds_t tempo_espera = 500000;
     usleep(tempo_espera);
 }
 
 char* concatenar_dois_chars(char* str1, char* str2){
 	char* resp = (char *) malloc(1 + strlen(str1)+ strlen(str2));
 	strcpy(resp, str1);
 	strcat(resp, str2);
 	return resp;
 }
 
 Querry_struct cria_comando_reader(char* tabela, char* cols, char* clausulawhere){
 	Querry_struct qs;
 	int i;
 	char* resp = concatenar_dois_chars("SELECT ", cols);
 	resp = concatenar_dois_chars(resp, " FROM ");
 	resp = concatenar_dois_chars(resp, tabela);
 	if(strcmp(clausulawhere, "") != 0){
 		resp = concatenar_dois_chars(resp, " WHERE ");
 		resp = concatenar_dois_chars(resp, clausulawhere);
 	}
 	resp = concatenar_dois_chars(resp, ";");
 	qs.querry = resp;
 	if (strcmp(tabela, "NERV") == 0)
 		qs.tabelatolock = 0;
 	else if (strcmp(tabela, "SEELE") == 0)
 		qs.tabelatolock = 1;
 	else if (strcmp(tabela, "WILLE") == 0)
 		qs.tabelatolock = 2;
 	if (strcmp(cols, "*") == 0)
 		for (i = 0;i < 2; i++)
 			qs.colunatolock[i] = 1;
 	else if (strcmp(cols, "EVAno") == 0){
 		qs.colunatolock[0] = 1;
 		qs.colunatolock[1] = 0;
 	}else if (strcmp(cols, "pilot") == 0){
 		qs.colunatolock[0] = 0;
 		qs.colunatolock[1] = 1;
 	}
 	return qs;
 }
 
 Querry_struct cria_comando_writer(char* tabela,  char* valor1, char* valor2){
 	Querry_struct qs;
 	int pos = rand() % (sizeof(writerquerries) / sizeof(writerquerries[0]));
 	int hastwo = rand() & 1;
 	char* comando = writerquerries[pos];
 	char* comandohelper = writerquerrieshelper[pos];
 	char* resp = concatenar_dois_chars(comando, tabela);
 	if (strcmp(comando, "CREATE TABLE ") == 0){
 		resp = concatenar_dois_chars(resp, create_table_atributes);
 	}else if (strcmp(comando, "DROP TABLE ") == 0);
 	else if (strcmp(comando, "UPDATE ") == 0){
 		char* where = concatenar_dois_chars("EVAno", wherehelper[rand() % (sizeof(wherehelper) / sizeof(wherehelper[0]))]);
 		where = concatenar_dois_chars(where, EVAs[rand() % (sizeof(EVAs) / sizeof(EVAs[0]))]);
 		resp = concatenar_dois_chars(resp, comandohelper);
 		if(hastwo){
	 		resp = concatenar_dois_chars(resp, "EVAno");
	 		resp = concatenar_dois_chars(resp, " = ");
	 		resp = concatenar_dois_chars(resp, valor1);
	 		resp = concatenar_dois_chars(resp, ", ");
 		}
 		resp = concatenar_dois_chars(resp, "pilot");
 		resp = concatenar_dois_chars(resp, " = '");
 		resp = concatenar_dois_chars(resp, valor2);
 		resp = concatenar_dois_chars(resp, "' WHERE ");
 		resp = concatenar_dois_chars(resp, where);
 	}else if (strcmp(comando, "INSERT INTO ") == 0){//INSERT
 		resp = concatenar_dois_chars(resp, comandohelper);
 		char* valores = concatenar_dois_chars("(", valor1);
 		valores = concatenar_dois_chars(valores, ", '");
 		valores = concatenar_dois_chars(valores, valor2);
 		valores = concatenar_dois_chars(valores, "')");
 		resp = concatenar_dois_chars(resp, valores);
 	}
 	resp = concatenar_dois_chars(resp, ";");
 	qs.querry = resp;
 	if (strcmp(tabela, "NERV") == 0)
 		qs.tabelatolock = 0;
 	else if (strcmp(tabela, "SEELE") == 0)
 		qs.tabelatolock = 1;
 	else if (strcmp(tabela, "WILLE") == 0)
 		qs.tabelatolock = 2;
 	int i;
 	if(!hastwo){
	 	qs.colunatolock[0] = 0;
	 	qs.colunatolock[1] = 1;
 	}else{
	 	for (i = 0;i < 2; i++)
	 		qs.colunatolock[i] = 1;
 	}
 	return qs;
 }

void lock_posicoes_matriz(int tabela, int colunas[2], int tid, int type){
	int cond = 0;
 	do{
 		int i, pos_parada;
 		cond = 0;
	 	for (i = 0;i < 2; i++){
	 		if(colunas[i] == 1){
	 			int check = pthread_mutex_trylock(&mutex_general[tabela][i]);
	 			if(check == 22){//EINVAL, not initialized
	 				pthread_mutex_init(&mutex_general[tabela][i], NULL);
	 				check = pthread_mutex_trylock(&mutex_general[tabela][i]);
	 			}
		 		if(check != 0){
		 			pos_parada = i;
		 			cond =1;
		 			printf("CheckLock %d/%d: %d - %d : %d\n", tid, type, check, tabela, i);
		 		}
	 		}
	 	}
	 	if(cond){
		 	for (i = 0;i < pos_parada; i++){
		 		if(colunas[i] == 1){
		 			pthread_mutex_unlock(&mutex_general[tabela][i]);
		 		}
		 	}
		 	sleep(rand() & 2);
		}
 	}while(!cond);
 }

 void unlock_posicoes_matriz(int tabela, int colunas[2]){
 	int i;
 	for (i = 0;i < 2; i++){
 		if(colunas[i] == 1){
 			pthread_mutex_unlock(&mutex_general[tabela][i]);
 			pthread_mutex_destroy(&mutex_general[tabela][i]);
 		}
 	}
 }

void lightswitch_lock(int number, pthread_mutex_t mutex, sem_t semaphore){
	pthread_mutex_lock(&mutex);
	number++;
	if(number == 1) {
		sem_wait(&semaphore);
	}
	pthread_mutex_unlock(&mutex);
}

void lightswitch_unlock(int number, pthread_mutex_t mutex, sem_t semaphore){
	pthread_mutex_lock(&mutex);
	number--;
	if(number == 0) {
		sem_post(&semaphore);
	}
	pthread_mutex_unlock(&mutex);
}
 
 void atualizar_tempos(int type, long int duration, pthread_mutex_t mutex){
 	if(type == 1){
 		pthread_mutex_lock(&mutex);
 		contadorVezesReader++;
 		mediaReader += duration;
 		if(duration >= maxReader)
 			maxReader = duration;
 		else
 			minReader = duration;
 		pthread_mutex_unlock(&mutex);
 	}else{
 		pthread_mutex_lock(&mutex);
 		contadorVezesWriter++;
 		mediaWriter += duration;
 		if(duration >= maxWriter)
 			maxWriter = duration;
 		else
 			minWriter = duration;
 		pthread_mutex_unlock(&mutex);
 	}
 }
 
 /* Funcao pra checar o erro mais facilmente, nao queria copiar isso em cada singela
  * querry que eu tinha feito.. ._. */
 void checarError(int rc, char* errorMessage){
 	if( rc!=SQLITE_OK ){
 	  fprintf(stderr, "SQL error: %s\n", errorMessage);
 	  sqlite3_free(errorMessage);
 	}
 }
 
 void* writer(void* numerothread) {
 	struct timeval tempoantes, tempodepois;
 	int duration, rc;
 	int writerId = *((int*) numerothread);
 	char *errorMessage = 0;
 	sem_post(&espera_inicio_thread);
 	do{
 		espera_tempo();

 		if(opcao_ts == 2){//"Solucao" usando a solucao do P2, o lightswitch
	 		pthread_mutex_lock(&mutex_noWriters);
			numWriter++;
				if(numWriter == 1)
					pthread_mutex_lock(&noReaders);
			pthread_mutex_unlock(&mutex_noWriters);
			pthread_mutex_lock(&noWriters);
		}
 
 		char* tabsw =  tables[rand() % (sizeof(tables) / sizeof(tables[0]))];
 		char* valor1 =  EVAs[rand() % (sizeof(EVAs) / sizeof(EVAs[0]))];
 		char* valor2 =  pilots[rand() % (sizeof(pilots) / sizeof(pilots[0]))];
 		Querry_struct qs = cria_comando_writer(tabsw, valor1, valor2);
 		
 		gettimeofday(&tempoantes, NULL);
 		lock_posicoes_matriz(qs.tabelatolock, qs.colunatolock, writerId, 1);
 		if(opcao_ts == 1)
 			pthread_mutex_lock(&mutex_exec);
 		printf("writer -> %d\n%s\n", writerId, qs.querry);
 		rc = sqlite3_exec(db, qs.querry, callback, 0, &errorMessage);
 		if(opcao_ts == 1)
 			pthread_mutex_unlock(&mutex_exec);
 		checarError(rc, errorMessage);
 		unlock_posicoes_matriz(qs.tabelatolock, qs.colunatolock);

 		if(opcao_ts == 2){//"Solucao" usando a solucao do P2, o lightswitch
	 		pthread_mutex_unlock(&noWriters);
		
			pthread_mutex_lock(&mutex_noWriters);
			numWriter--;
				if(numWriter==0)
					pthread_mutex_unlock(&noReaders);
			pthread_mutex_unlock(&mutex_noWriters);
		}
 		
 		gettimeofday(&tempodepois, NULL);
 		duration = tempodepois.tv_usec - tempoantes.tv_usec;
 		atualizar_tempos(0, duration, mutex_time_data_writer);
 		
 	}while (varCond < 1);
 	sem_post(&espera_fim_thread);
 	printf("Thread-Writer %d termianda!\n", writerId);
    return NULL;
 }
 
 void* reader(void* numerothread) {
 	struct timeval tempoantes, tempodepois;
 	int duration, rc;
 	int readerId = *((int*) numerothread);
 	char *errorMessage = 0;
 	sem_post(&espera_inicio_thread);
 	do{
 		espera_tempo();
 		if(opcao_ts == 2){//"Solucao" usando a solucao do P2, o lightswitch
	 		pthread_mutex_lock(&noReaders);
			pthread_mutex_lock(&mutex_noReaders);
				numReader++;
				if(numReader==1)
					pthread_mutex_lock(&noWriters);
			pthread_mutex_unlock(&mutex_noReaders);
			pthread_mutex_unlock(&noReaders);
		}

 		char* tabsr =  tables[rand() % (sizeof(tables) / sizeof(tables[0]))];
 		char* colsr =  colunas[rand() % (sizeof(colunas) / sizeof(colunas[0]))];
 		char* where = "";
 		if(rand() & 1){
 			where = concatenar_dois_chars("EVAno", wherehelper[rand() % (sizeof(wherehelper) / sizeof(wherehelper[0]))]);
 			where = concatenar_dois_chars(where, EVAs[rand() % (sizeof(EVAs) / sizeof(EVAs[0]))]);
 		}
 		Querry_struct qs = cria_comando_reader(tabsr, colsr, where);
 		
 		gettimeofday(&tempoantes, NULL);
 		lock_posicoes_matriz(qs.tabelatolock, qs.colunatolock, readerId, 2);
 		if(opcao_ts == 1)//Solucao do mutex_exec, executa linearmente os execs
 			pthread_mutex_lock(&mutex_exec);
 		printf("reader -> %d\n%s\n", readerId, qs.querry);
 		rc = sqlite3_exec(db, qs.querry, callback, 0, &errorMessage);
 		if(opcao_ts == 1)
 			pthread_mutex_unlock(&mutex_exec);
 		checarError(rc, errorMessage);
 		unlock_posicoes_matriz(qs.tabelatolock, qs.colunatolock);

		if(opcao_ts == 2){
 			pthread_mutex_lock(&mutex_noReaders);
				numReader--;
				if(numReader==0)
					pthread_mutex_unlock(&noWriters);
			pthread_mutex_unlock(&mutex_noReaders);
		}
 		
 		gettimeofday(&tempodepois, NULL);
 		duration = tempodepois.tv_usec - tempoantes.tv_usec;
 		atualizar_tempos(1, duration, mutex_time_data_reader);
 		
 	}while(varCond < 1);
 	sem_post(&espera_fim_thread);
 	printf("Thread-Reader %d termianda!\n", readerId);
    return NULL;
 }
 
 void handler (int signal_number){
 	switch (signal_number) {
 	case SIGINT:
 		printf("CTRL+C foi digitada pelo usuario....\n");
 		varCond = 1;
 		int v;
 		for(v=1;v <= (tantosReader+tantosWriter);v++)
 			sem_wait(&espera_fim_thread);
 		sleep(1);
 		printf("Medias e tempos, todos em microssegundos:\n");
 		mediaWriter = (double)mediaWriter/contadorVezesWriter;
 		printf("Escritor => Media: %.2f, Max: %.0f e Min: %.0f\n", mediaWriter, maxWriter, minWriter);
 		mediaReader = (double)mediaReader/contadorVezesReader;
 		printf("Leitor => Media: %.2f, Max: %.0f e Min: %.0f\n", mediaReader, maxReader, minReader);
 		char *errorMessage = 0;
 		int rc = sqlite3_exec(db, querryfim, callback, 0, &errorMessage);
 		checarError(rc, errorMessage);
 		sqlite3_close(db);
 		printf("Programa terminado!\n");
 		break;
 	}
 }
 
 int main(int argc, char *argv[]){
 	if(argc < 3){
         printf("Como usar: %s <numero reader> <numero writers> <opcao de threadsafe>\n", argv[0]);
         return 0;
     }
     opcao_ts = argv[3] ? atoi(argv[3]) : 0;
     tantosWriter = atoi(argv[2]);
     tantosReader = atoi(argv[1]);
    pthread_t threads_reader[tantosReader], threads_writer[tantosWriter];
 	struct sigaction sa;
 	memset (&sa, 0, sizeof (sa));
 	sa.sa_handler = &handler;
 	sigaction (SIGINT, &sa, NULL);
 	sem_init(&espera_inicio_thread,0,0);
 	sem_init(&espera_fim_thread,0,0);
 	srand(time(NULL));
 	int rc;//O int e o error-handler, o sqlite retorna um numero como numa requisicao HTTP.
 	rc = sqlite3_open(":memory:", &db);//Cria o banco de dados na memoria.
 	if(rc){//Um error-handler
 	  fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
 	  sqlite3_close(db);
 	  return 1;
 	}
 	char *errorMessage = 0;
 	rc = sqlite3_exec(db, querryini, callback, 0, &errorMessage);
 	checarError(rc, errorMessage);
 	int i, k;
 	
	for(i=0;i < 3;i++)
		for(k=0;k < 2;k++)
			pthread_mutex_init(&mutex_general[i][k], NULL);
 
 	for(i=1;i <= tantosReader;i++){
 		pthread_create(&threads_reader[i], NULL, &reader, &i);
 		sem_wait(&espera_inicio_thread);
 	}
 	for(i=1;i <= tantosWriter;i++){
 		pthread_create(&threads_writer[i], NULL, &writer, &i);
 		sem_wait(&espera_inicio_thread);
 	}
 	for(i=1;i <= tantosReader;i++)
 		pthread_join(threads_reader[i], NULL);
 	for(i=1;i <= tantosWriter;i++)
 		pthread_join(threads_writer[i], NULL);
	for(i=0;i < 3;i++)
		for(k=0;k < 2;k++)
			pthread_mutex_destroy(&mutex_general[i][k]);

 	pthread_exit(NULL);
 	sqlite3_close(db);//Tem que fechar o banco de dados quando termina!
 	return 0;
 }